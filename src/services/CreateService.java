package services;

import enums.AccountType;
import models.Account;

import java.util.Date;
import java.util.Random;

import static services.AccountService.*;
import static services.DisplayService.clearScreen;
import static services.DisplayService.returnToMainMenu;

public class CreateService {

    public static void createAccount() {
        DisplayService.displayAccountTypes();

        System.out.print("Choose an account type: ");
        int typeChoice = scanner.nextInt();
        AccountType accountType = DisplayService.getAccountType(typeChoice);

        System.out.println("Enter account details:");

        System.out.print("Name: ");
        String name = scanner.next();

        double balance;
        double minimumBalance;

        do {
            System.out.print("Initial Balance: ");
            balance = scanner.nextDouble();

            minimumBalance = getMinimumBalance(accountType);

            if (balance < minimumBalance) {
                System.out.println("The minimum initial balance for this account type must be more than " + minimumBalance + ". Please try again");
            }

        } while (balance < minimumBalance);

        Account newAccount = new Account(name, generateRandomAccountNumber(), new Date(), balance, minimumBalance, accountType);
        accounts.add(newAccount);

        clearScreen();
        System.out.println("Account created successfully!");

        returnToMainMenu();
    }

    private static int generateRandomAccountNumber() {
        var random = new Random();
        return 100000 + random.nextInt(900000);
    }

    static double getMinimumBalance(AccountType accountType) {
        return switch (accountType) {
            case CURRENT_ACCOUNT -> 35000.0;
            case SAVINGS_ACCOUNT -> 6800.0;
            case SALARY_ACCOUNT -> 40000.0;
            default -> 0.0;
        };
    }
}
