package services;

import models.Account;

import static services.AccountService.accounts;
import static services.AccountService.scanner;
import static services.CreateService.getMinimumBalance;
import static services.DisplayService.*;

public class UpdateService {
    public static void updateAccount() {
        if (accounts.isEmpty()) {
            System.out.println("No accounts available for update.");
            return;
        }

        System.out.print("Enter the account number to update: ");
        int accountNumberToUpdate = scanner.nextInt();

        Account accountToUpdate = findAccountByNumber(accountNumberToUpdate);

        if (accountToUpdate == null) {
            System.out.println("Account not found.");
            return;
        }

        System.out.println("Enter updated account details:");

        System.out.print("Name: ");
        accountToUpdate.setName(scanner.next());

        System.out.print("Initial Balance: ");
        double updatedBalance = scanner.nextDouble();
        double minimumBalance = getMinimumBalance(accountToUpdate.getAccountType());

        // Validate the updated balance against the minimum balance
        while (updatedBalance < minimumBalance) {
            System.out.println("Updated balance is below the minimum required balance.");
            System.out.print("Enter the updated initial balance: ");
            updatedBalance = scanner.nextDouble();
        }

        accountToUpdate.setBalance(updatedBalance);

        clearScreen();
        System.out.println("Account updated successfully!");

        returnToMainMenu();
    }
}
