package services;

import enums.AccountType;
import models.Account;

import java.text.SimpleDateFormat;
import java.util.Date;

import static services.AccountService.accounts;
import static services.AccountService.scanner;

public class DisplayService {
    static void displayAccountTypes() {
        System.out.println("1. Current Account");
        System.out.println("2. Savings Account");
        System.out.println("3. Salary Account");
    }

    public static void displayMenu() {
        System.out.println("1. Create a new account");
        System.out.println("2. Display all accounts");
        System.out.println("3. Update an account");
        System.out.println("4. Delete an account");
        System.out.println("5. Deposit an amount into your account");
        System.out.println("6. Withdraw an amount from your account");
        System.out.println("7. Search for account");
        System.out.println("0. Exit");
    }

    static AccountType getAccountType(int choice) {
        return switch (choice) {
            case 1 -> AccountType.CURRENT_ACCOUNT;
            case 2 -> AccountType.SAVINGS_ACCOUNT;
            case 3 -> AccountType.SALARY_ACCOUNT;
            default -> {
                System.out.println("Invalid choice. Defaulting to Current Account.");
                yield AccountType.CURRENT_ACCOUNT;
            }
        };
    }

    public static void displayAllAccounts() {
        if (accounts.isEmpty()) {
            System.out.println("No accounts available.");
            return;
        }

        System.out.println("All Accounts:");

        for (Account account : accounts) {
            displayAccountDetails(account);
        }

        returnToMainMenu();
    }

    static Account findAccountByNumber(int accountNumber) {
        for (Account account : accounts) {
            if (account.getNumber() == accountNumber) {
                return account;
            }
        }
        return null;
    }

    static void displayAccountDetails(Account account) {
        System.out.println("Account Number: " + account.getNumber());
        System.out.println("Account Holder: " + account.getName());
        System.out.println("Account Type: " + account.getAccountType());
        System.out.println("Creation Date: " + formatDate(account.getCreationDate()));
        System.out.println("Current Balance: " + account.getBalance());
        System.out.println("-------------------------------");
    }

    public static void returnToMainMenu() {
        System.out.println("Press 0 to return to the main menu.");
        int choice = scanner.nextInt();

        while (choice != 0) {
            System.out.println("Invalid choice. Please press 0 to return to the main menu.");
            choice = scanner.nextInt();
        }
        clearScreen();
        displayMenu();
    }

    private static String formatDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(date);
    }

    static void clearScreen() {
        try {
            if (System.getProperty("os.name").contains("Windows")) {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            } else {
                System.out.print("\033[H\033[2J");
                System.out.flush();
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
