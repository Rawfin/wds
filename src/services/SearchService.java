package services;

import models.Account;

import java.util.ArrayList;

import static services.AccountService.*;
import static services.DisplayService.displayAccountDetails;
import static services.DisplayService.returnToMainMenu;

public class SearchService {
    public static void searchAccount() {
        if (accounts.isEmpty()) {
            System.out.println("No accounts available for searching.");
            return;
        }

        System.out.print("Enter a partial account holder's name to search: ");
        String partialNameToSearch = scanner.next();

        ArrayList<Account> matchingAccounts = findAccountsByPartialName(partialNameToSearch);

        if (matchingAccounts.isEmpty()) {
            System.out.println("No accounts found with the given partial name.");
        } else {
            System.out.println("Accounts found:");
            for (Account foundAccount : matchingAccounts) {
                displayAccountDetails(foundAccount);
            }
        }

        returnToMainMenu();
    }


    private static ArrayList<Account> findAccountsByPartialName(String partialName) {
        ArrayList<Account> matchingAccounts = new ArrayList<>();

        for (Account account : accounts) {
            if (account.getName().toLowerCase().contains(partialName.toLowerCase())) {
                matchingAccounts.add(account);
            }
        }

        return matchingAccounts;
    }
}
