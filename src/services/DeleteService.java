package services;

import models.Account;

import static services.AccountService.accounts;
import static services.AccountService.scanner;
import static services.DisplayService.*;
import static services.DisplayService.displayMenu;

public class DeleteService {
        public static void deleteAccount() {
        if (accounts.isEmpty()) {
            System.out.println("No accounts available for deletion.");
            return;
        }

        System.out.print("Enter the account number to delete: ");
        int accountNumberToDelete = scanner.nextInt();

        Account accountToDelete = findAccountByNumber(accountNumberToDelete);

        if (accountToDelete == null) {
            System.out.println("Account not found.");
            return;
        }

        System.out.println("Are you sure you want to delete the following account?");
        displayAccountDetails(accountToDelete);
        System.out.print("Enter 'Y' to confirm deletion, or any other key to cancel: ");
        String confirmation = scanner.next();

        clearScreen();
        if (confirmation.equalsIgnoreCase("Y")) {
            accounts.remove(accountToDelete);
            System.out.println("Account deleted successfully!");
        } else {
            System.out.println("Deletion canceled.");
        }

        returnToMainMenu();
    }
}
