package services;

import enums.AccountType;
import models.Account;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;

import static services.DisplayService.*;

public class AccountService {
    static ArrayList<Account> accounts = new ArrayList<>();
    static Scanner scanner = new Scanner(System.in);

    public static void depositAmount() {
        clearScreen();
        if (accounts.isEmpty()) {
            System.out.println("No accounts available for deposit.");
            displayMenu();
            return;
        }

        System.out.print("Enter the account number to deposit into: ");
        int accountNumberToDeposit = scanner.nextInt();

        Account accountToDeposit = findAccountByNumber(accountNumberToDeposit);

        if (accountToDeposit == null) {
            System.out.println("Account not found.");
            return;
        }

        System.out.print("Enter the amount to deposit: ");
        double depositAmount = scanner.nextDouble();

        // Validate deposit amount (positive value)
        while (depositAmount <= 0) {
            System.out.println("Invalid deposit amount. Please enter a positive value.");
            System.out.print("Enter the amount to deposit: ");
            depositAmount = scanner.nextDouble();
        }

        // Perform the deposit
        double updatedBalance = accountToDeposit.getBalance() + depositAmount;
        accountToDeposit.setBalance(updatedBalance);

        System.out.println("Deposit successful!");
        displayAccountDetails(accountToDeposit);
    }

    public static void withdrawAmount() {
        clearScreen();
        if (accounts.isEmpty()) {
            System.out.println("No accounts available for withdrawal.");
            displayMenu();
            return;
        }

        System.out.print("Enter the account number to withdraw from: ");
        int accountNumberToWithdraw = scanner.nextInt();

        Account accountToWithdraw = findAccountByNumber(accountNumberToWithdraw);

        if (accountToWithdraw == null) {
            System.out.println("Account not found.");
            return;
        }

        System.out.print("Enter the amount to withdraw: ");
        double withdrawAmount = scanner.nextDouble();

        // Validate withdraw amount (positive value)
        while (withdrawAmount <= 0 || withdrawAmount > accountToWithdraw.getBalance()) {
            if (withdrawAmount <= 0) {
                System.out.println("Invalid withdraw amount. Please enter a positive value.");
            } else {
                System.out.println("Insufficient funds. Cannot withdraw more than the available balance.");
            }

            System.out.print("Enter the amount to withdraw: ");
            withdrawAmount = scanner.nextDouble();
        }

        // Perform the withdrawal
        double updatedBalance = accountToWithdraw.getBalance() - withdrawAmount;
        accountToWithdraw.setBalance(updatedBalance);

        System.out.println("Withdrawal successful!");
        displayAccountDetails(accountToWithdraw);
    }
}
