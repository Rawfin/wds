package models;

import enums.AccountType;
import java.util.Date;

public class Account {
    private String name;
    private int number;
    private Date creationDate;
    private double balance;
    private AccountType accountType;

    public Account(String name, int number, Date creationDate, double balance, double minimumBalance, AccountType accountType) {
        this.name = name;
        this.number = number;
        this.creationDate = creationDate;
        this.balance = balance;
        this.accountType = accountType;
    }

    public String getName() {
        return name;
    }

    public int getNumber() {
        return number;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public double getBalance() {
        return balance;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }
}
