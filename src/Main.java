import services.*;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);

        int choice;

        do {
            DisplayService.displayMenu();

            System.out.print("Enter your choice: ");
            choice = scanner.nextInt();

            switch (choice) {
                case 1 -> CreateService.createAccount();
                case 2 -> DisplayService.displayAllAccounts();
                case 3 -> UpdateService.updateAccount();
                case 4 -> DeleteService.deleteAccount();
                case 5 -> AccountService.depositAmount();
                case 6 -> AccountService.withdrawAmount();
                case 7 -> SearchService.searchAccount();
                case 0 -> System.out.println("Exiting the application. Goodbye!");
                default -> System.out.println("Invalid choice. Please enter a number between 1 and 8.");
            }
        } while (choice != 8);
    }
}
